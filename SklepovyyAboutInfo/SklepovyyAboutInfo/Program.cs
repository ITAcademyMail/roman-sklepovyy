﻿using System;

namespace SklepovyyAboutInfo
{
    class Program
    {
        static void Main(string[] args)
        {
            string first_name = "Roman";
            string last_name = "Sklepovyy";

            ShowName(first_name);
            ShowSurname(last_name);
          
           
        }

        static void ShowName(string name)
        {
            string input;
            bool exit = false;

            Console.WriteLine("Do you want to know my name? (Y)es/(N)o:");

            while (!exit)
            {
                input = Console.ReadLine();

                if (input.Length > 1)
                {
                    Console.WriteLine("Too many charecters! Try again!");
                }
                else if (input == "y" || input == "Y")
                {
                    Console.WriteLine("My name is " + name + "!");
                    exit = true;
                } else if (input == "n" || input == "N") {
                    Console.WriteLine("Okay. Goodbye..");
                    exit = true;
                }
                else Console.WriteLine("Wrong character! Try again!");
            }
        }

        static void ShowSurname(string surname)
        {
            string input;
            bool exit = false;

            Console.WriteLine("\nMaybe you wanna know my surname too ? (Y)es/(N)o:");

            while (!exit)
            {
                input = Console.ReadLine();

                if (input.Length > 1)
                {
                    Console.WriteLine("Too many charecters! Try again!");
                }
                else if (input == "y" || input == "Y")
                {
                    Console.WriteLine("My surname is " + surname + "! Goodbye)");
                    exit = true;
                }
                else if (input == "n" || input == "N")
                {
                    Console.WriteLine("Okay. Goodbye..");
                    exit = true;
                }
                else Console.WriteLine("Wrong character! Try again!");
            }
        }
    }
}
